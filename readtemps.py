import subprocess
import glob
import time
import string
import datetime
from Adafruit_IO import *

aio = Client('e3cc2b33bea04c228853ba3670bd1ae2')
#aio.send('Foo', 100)


#base_dir = '/sys/bus/w1/devices/'
#device_list = glob.glob(base_dir + '28*')

#Letters are written on temp sensors.
serials = {
    'A':'28-0316b15f80ff',
    'B':'28-0516b18a85ff',
    'C':'28-0316b178a7ff',
    'D':'28-0516b187dbff',
    'E':'28-0316b15e08ff',
    'F':'28-0316b15967ff',
    'G':'28-0516b18c7cff',
    'H':'28-0316b1806cff',
    'I':'28-0316b179d3ff',
    'J':'28-0316b17baeff'
}

addresses = {
    'A':'/sys/bus/w1/devices/28-0316b15f80ff/w1_slave',
    'B':'/sys/bus/w1/devices/28-0516b18a85ff/w1_slave',
    'C':'/sys/bus/w1/devices/28-0316b178a7ff/w1_slave',
    'D':'/sys/bus/w1/devices/28-0516b187dbff/w1_slave',
    'E':'/sys/bus/w1/devices/28-0316b15e08ff/w1_slave',
    'F':'/sys/bus/w1/devices/28-0316b15967ff/w1_slave',
    'G':'/sys/bus/w1/devices/28-0516b18c7cff/w1_slave',
    'H':'/sys/bus/w1/devices/28-0316b1806cff/w1_slave',
    'I':'/sys/bus/w1/devices/28-0316b179d3ff/w1_slave',
    'J':'/sys/bus/w1/devices/28-0316b17baeff/w1_slave',
}

# def get_serial_numbers():
#     serials = []
#     addresses = []
#     for i in range(0, len(device_list)):
#         serials.append(device_list[i].replace(base_dir,''))
#         addresses.append(device_list[i] + '/w1_slave')
#     return addresses, serials

def list_devices():
    for sensor in sorted(serials):
		print('Sensor ' + sensor + ' SN:' + serials[sensor])

def read_temp(sensor):
    catdata = subprocess.Popen(['cat', addresses[sensor]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = catdata.communicate()
    out_decode = out.decode('utf-8')
    lines = out_decode.split('\n')
    # while lines[0].strip()[-3:] != 'YES':
    #     time.sleep(0.2)
    #     lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f

def print_all_temps():
    print('Sensor ID | Temp F | Temp C')
    for sensor in sorted(serials):
        temp_c, temp_f = read_temp(sensor)
        print('{:^10}'.format(sensor) + '| ' + '{:7.2f}'.format(temp_f) + '|' + '{:7.2f}'.format(temp_c))

def send_to_aio():
    for sensor in sorted(serials):
        temp_c, temp_f = read_temp(sensor)
        feed_name = 'sensor_' + sensor
        try:
            aio.send(feed_name,temp_f)
        except:
            print(datetime.datetime.now().strftime("%y-%m-%d-%H-%M") + ': ' + feed_name + ':ERROR sending to Adafruit IO')
        else:
            print(datetime.datetime.now().strftime("%y-%m-%d-%H-%M") + ': ' + feed_name + ':Sent ' + '{:7.2f}'.format(temp_f))
    print('------------------------------------')

#list_devices()
#print_all_temps()
while 1:
    send_to_aio()
    time.sleep(60)
